# Saarmeisen Mapeditor

Tool um einfach maps in Saarmeisen format zu bauen.
Alle exportierten maps werden öffentlich auf [saarmeisen.soontm.net](https://saarmeisen.soontm.net) hochgeladen damit wir sie alle teilen können.
Dafür sollte man zuerst noch seinen Namen im Menü einstellen.
Es gibt 64 bit builds für Linux, Mac und Windows. Mac wurde noch nie getestet. 

Bei Linux gab es Probleme bei der Texteingabe sowie einer Combobox. Meldet euch, falls das bei euch auch der Fall ist oder Ahnung davon habt bei s8addapp@stud.uni-saarland.de

## Benutzung
* Linksclick um auf ein Feld zu malen
* Rechtsclick oder Strg+Linksclick um Kamera zu verschieben

* Export Map speichert map in einem File und lädt sie an o.g. Adresse hochg
* Load Map lädt eine vorhandene Map um weiter zu editieren

## Features
* Erkennt wenn Bases nicht zusammenhängend sind und wenn ein Schwarm Buchstabe ausgelassen wurde.
* Übersicht auf Verteilung der einzelnen Felder, totales Food auf der map, wie viele Bases pro Schwarm.
* Symmetrie optionen um schön symmetrische Maps zu bauen.