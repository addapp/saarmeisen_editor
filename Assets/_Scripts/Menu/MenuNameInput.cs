﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(TMP_InputField))]
public class MenuNameInput : MonoBehaviour {

	private string name;
	private static readonly string defaultName = "Saarmeise";
	private TMP_InputField inputField;

	private void Awake() {
		inputField = GetComponent<TMP_InputField>();
		name = PlayerPrefs.GetString("author");
		inputField.text = name;
	}
	
	public void ChangeName(string name) {
		if (name.Equals("")) {
			inputField.text = this.name;
		} else {
			this.name = name;
			PlayerPrefs.SetString("author", name);
			EventManager.TriggerEvent("setAuthor");
		}
	}
}
