﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class MenuClose : MonoBehaviour {

	[SerializeField] private GameObject menuPanel;
	private Button button;
	
	private void Awake() {
		button = GetComponent<Button>();
	}

	private void Start() {
		EventManager.StartListening("setAuthor", () => button.enabled = true);
		if (PlayerPrefs.HasKey("author")) {
			button.enabled = true;
			menuPanel.SetActive(false);
		}
	}

	public void CloseMenu() {
		menuPanel.SetActive(false);
	}
}
