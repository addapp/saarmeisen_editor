﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class MenuInvertCamera : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Toggle>().isOn = PlayerPrefs.HasKey("invertCamera");
	}

	public void ChangeValue(bool value) {
		if (value) {
			PlayerPrefs.SetInt("invertCamera", 1);
		} else {
			PlayerPrefs.DeleteKey("invertCamera");
		}
		
	}
}
