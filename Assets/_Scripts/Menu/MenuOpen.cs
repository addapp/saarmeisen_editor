﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuOpen : MonoBehaviour {

	[SerializeField] private GameObject menuPanel;

	public void OpenMenu() {
		menuPanel.SetActive(true);
	}
}
