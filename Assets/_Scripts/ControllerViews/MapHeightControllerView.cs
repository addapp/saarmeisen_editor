﻿using System;

public class MapHeightControllerView : InputFieldControllerView<Map> {

	protected override bool CreateSpecific (string height, out Command cmd) {
		int h;
		try {
			h = int.Parse(height);
		} catch (Exception e) when (e is FormatException || e is OverflowException) {
			cmd = null;
			return false;
		}

		if (h < Map.MIN_DIM || h > Map.MAX_DIM || h % 2 == 1) {
			cmd = null;
			return false;
		}


		cmd = new MapResizeHeightCommand(h);
		return true;
	}

	public override void Notify (Map map) {
		inputField.text = map.GetHeight().ToString();
	}
}
