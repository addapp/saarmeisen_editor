﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public abstract class ToggleControllerView<T> : ControllerView<T> where T : Model<T> {

	[SerializeField]
	protected Toggle toggle;

	protected override void OnAwake () {
		toggle = GetComponent<Toggle>();
	}

	public void Create (bool value) {
		executor.AddCommand(CreateSpecific(value));
	}

	protected abstract Command CreateSpecific (bool value);
}
