﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapNewController : ButtonController
{
	protected override void Create () {
		executor.AddCommand(new MapNewCommand());
	}
}
