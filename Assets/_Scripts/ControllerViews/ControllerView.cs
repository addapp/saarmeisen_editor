﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ControllerView<T> : MonoBehaviour where T : Model<T>
{

	protected CommandExecutor executor;


	/// <summary>
	/// Set the executor and register this controller at the appropriate model. 
	/// ATTENTION: This requires every model to be tagged with a tag that has the same name as its class name. Eg gameobject of class Map tagged with "Map"
	/// Controllers for Models that don't adhere to this must register themselves in some other way
	/// Also is a subclass wants to do some setup it should override OnAwake.
	/// </summary>
	protected void Awake () {
		executor = GameObject.FindGameObjectWithTag("CommandExecutor").GetComponent<CommandExecutor>();

		string tag = typeof(T).Name;
		GameObject model = GameObject.FindGameObjectWithTag(tag);
		Debug.Log("Trying to register at model " + tag);

		if (model != null) {
			Debug.Log("Successfully registered at model " + tag);
			model.GetComponent<T>().Register(this);
		} else {
			Debug.Log("Could not find model with tag: " + tag);
		}

		OnAwake();
	}

	protected virtual void OnAwake () { }
	public abstract void Notify (T value);
}
