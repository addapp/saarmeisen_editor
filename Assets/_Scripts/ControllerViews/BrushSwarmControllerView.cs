﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BrushSwarmControllerView : InputFieldControllerView<Brush> {
	
	public override void Notify (Brush brush) {
		inputField.text = brush.SwarmID.ToString();
	}

	protected override bool CreateSpecific (string swarm, out Command cmd) {
		if (swarm.Length != 1) {
			cmd = null;
			return false;
		} 

		char id = swarm[0];
		if (id < 'A' || id > 'Z') {
			cmd = null;
			return false;
		}

		cmd = new BrushChangeSwarmCommand(id);
		return true;
	}
}
