﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Dropdown))]
public abstract class DropdownControllerView<T> : ControllerView<T> where T : Model<T>
{
	protected Dropdown dropdown;

	protected override void OnAwake () {
		dropdown = GetComponent<Dropdown>();
	}

	public void Create(int value) {
		executor.AddCommand(CreateSpecific(value));
	}

	protected abstract Command CreateSpecific (int value);
}
