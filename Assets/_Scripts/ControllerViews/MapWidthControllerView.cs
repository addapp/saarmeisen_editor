﻿using System;

public class MapWidthControllerView : InputFieldControllerView<Map>
{
	protected override bool CreateSpecific (string width, out Command cmd) {
		int w;
		try {
			w = int.Parse(width);
		} catch (Exception e) when (e is FormatException || e is OverflowException) {
			cmd = null;
			return false;
		}

		if (w < Map.MIN_DIM || w > Map.MAX_DIM || w % 2 == 1) {
			cmd = null;
			return false;
		}


		cmd = new MapResizeWidthCommand(w);
		return true;
	}

	public override void Notify (Map map) {
		inputField.text = map.GetWidth().ToString();
	}

	public override string ToString () {
		return "I'm a width controller";
	}
}