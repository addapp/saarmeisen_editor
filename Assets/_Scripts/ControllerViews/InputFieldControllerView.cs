﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

[RequireComponent(typeof(TMP_InputField))]
public abstract class InputFieldControllerView<T> : ControllerView<T> where T : Model<T> {

	[SerializeField]
	protected TMP_InputField inputField;

	private string startValue;

	protected override void OnAwake () {
		inputField = GetComponent<TMP_InputField>();
		inputField.onSelect.AddListener(SelectInput);
		inputField.onEndEdit.AddListener(Create);
	}

	public void Create (string value) {
		EventManager.TriggerEvent("WriteToInputFieldStop");
		Command cmd;
		if (CreateSpecific(value, out cmd)) {
			startValue = value;
			executor.AddCommand(cmd);
		} else {
			inputField.text = startValue;
		}
	}

	protected abstract bool CreateSpecific (string value, out Command cmd);

	public void SelectInput (string value) {
		EventManager.TriggerEvent("WriteToInputFieldStart");
		startValue = value;
	}
}
