﻿using System.Linq;
using UnityEngine;
using System;
using System.Collections.Generic;

public class MapSymmetryControllerView : DropdownControllerView<Map>
{
	protected override void OnAwake () {
		base.OnAwake();
		List<string> options = ((Symmetry[])Enum.GetValues(typeof(Symmetry))).Select(s => s.ToString()).ToList();
		dropdown.AddOptions(options);
	}
	public override void Notify (Map map) {
		dropdown.value = (int)map.Symmetry;
	}

	protected override Command CreateSpecific (int value) {
		return new MapSymmetryCommand((Symmetry)value);
	}
}
