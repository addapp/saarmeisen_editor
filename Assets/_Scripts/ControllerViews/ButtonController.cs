﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public abstract class ButtonController : MonoBehaviour {
	protected CommandExecutor executor;

	private void Awake () {
		executor = GameObject.FindGameObjectWithTag("CommandExecutor").GetComponent<CommandExecutor>();
	}

	protected abstract void Create();
}
