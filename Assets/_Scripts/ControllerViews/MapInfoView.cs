﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text;
using System;
using System.Linq;

public class MapInfoView : ControllerView<Map> {

	[SerializeField] private TextMeshProUGUI foodText;
	[SerializeField] private TextMeshProUGUI fieldDistributionText;
	[SerializeField] private TextMeshProUGUI swarmDistributionText;
	[SerializeField] private TextMeshProUGUI contiguousWarningText;
	[SerializeField] private TextMeshProUGUI consecutiveWarningText;

	private int food;
	private bool lastSwarmID;
	private Dictionary<FieldType, int> fieldDistribution;
	private Dictionary<char, List<Field>> swarmDistribution;

	protected override void OnAwake () {
		Array fieldTypes = Enum.GetValues(typeof(FieldType));
		fieldDistribution = new Dictionary<FieldType, int>(fieldTypes.Length);
		swarmDistribution = new Dictionary<char, List<Field>>(26);

		foreach (FieldType fieldType in fieldTypes) {
			fieldDistribution.Add(fieldType, 0);
		}

		for (char swarmID = 'A'; swarmID <= 'Z'; swarmID++) {
			swarmDistribution.Add(swarmID, new List<Field>());
		}

		Init();
	}

	private void Init() {
		food = 0;
		lastSwarmID = false;
		foreach (FieldType fieldType in Enum.GetValues(typeof(FieldType))) {
			fieldDistribution[fieldType] = 0;
		}
		for (char swarmID = 'A'; swarmID <= 'Z'; swarmID++) {
			swarmDistribution[swarmID].Clear(); ;
		}
		contiguousWarningText.enabled = false;
		consecutiveWarningText.enabled = false;
	}

	public override void Notify (Map map) {
		Init();

		Field[,] fields = map.GetFields();

		for (int x = 0; x < map.GetWidth(); x++) {
			for (int y = 0; y < map.GetHeight(); y++) {
				Field field = fields[x, y];

				food += field.GetFood();
				fieldDistribution[field.GetFieldType()] = fieldDistribution[field.GetFieldType()] + 1;

				if (field.GetSwarmID() != Field.noSwarmId) {
					swarmDistribution[field.GetSwarmID()].Add(field);
				}
			}
		}

		foodText.text = "Total food: " + food;

		StringBuilder fieldDistributionBuilder = new StringBuilder();
		foreach (KeyValuePair<FieldType, int> entry in fieldDistribution) {
			fieldDistributionBuilder.Append(string.Format("{0}: {1}\n", entry.Key.ToString(), entry.Value));
		}
		fieldDistributionText.text = fieldDistributionBuilder.ToString();

		StringBuilder swarmDistributionBuilder = new StringBuilder();
		foreach (KeyValuePair<char, List<Field>> entry in swarmDistribution) {
			int count = entry.Value.Count;
			if (count != 0) {
				swarmDistributionBuilder.Append(string.Format("{0}: {1}\n", entry.Key.ToString(), count));

				// check for every list of bases if they are connected (if the list is non empty)
				if (!CheckContiguousBases(entry.Value, map)) {
					contiguousWarningText.enabled = true;
				}

				if (lastSwarmID) {
					consecutiveWarningText.enabled = true;
				}
			} else {
				lastSwarmID = true;
			}
		}
		swarmDistributionText.text = swarmDistributionBuilder.ToString();
	}

	private bool CheckContiguousBases (List<Field> bases, Map map) {		
		Queue<Field> toCheck = new Queue<Field>();
		toCheck.Enqueue(bases[0]);
		List<Field> checkedBases = new List<Field>();

		while (toCheck.Count != 0) {
			Field current = toCheck.Dequeue();
			if (checkedBases.Contains(current)) {
				continue;
			}

			if (bases.Remove(current)) {
				map.GetNeighbours(current).ForEach(neighbour => toCheck.Enqueue(neighbour));
				checkedBases.Add(current);
			}
		}

		return 0 == bases.Count;
	}
}
