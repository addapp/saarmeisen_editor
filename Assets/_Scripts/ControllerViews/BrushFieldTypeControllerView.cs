﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class BrushFieldTypeControllerView : ControllerView<Brush> {

	[SerializeField]
	Button rockButton;
	[SerializeField]
	Button normalButton;
	[SerializeField]
	Button antlionButton;
	[SerializeField]
	Button baseButton;

	public override void Notify (Brush value) {
		UnselectAll();

		switch(value.FieldType) {
			case FieldType.Normal:
				Select(normalButton);
				break;
			case FieldType.Antlion:
				Select(antlionButton);
				break;
			case FieldType.Base:
				Select(baseButton);
				break;
			case FieldType.Rock:
				Select(rockButton);
				break;
		}
	}


	//todo change sprite instead of / in addition to rotating
	private void UnselectAll() {
		rockButton.transform.rotation = Quaternion.identity;
		normalButton.transform.rotation = Quaternion.identity;
		antlionButton.transform.rotation = Quaternion.identity;
		baseButton.transform.rotation = Quaternion.identity;
	}

	private void Select(Button button) {
		button.transform.rotation = Quaternion.Euler(0, 0, 30);
	}

	public void CreateChangeFieldCommand(string type) {
		FieldType fieldType;
		Enum.TryParse<FieldType>(type, true, out fieldType);

		executor.AddCommand(new BrushChangeFieldCommand(fieldType));
	}
}
