﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrushFoodControllerView : InputFieldControllerView<Brush> {

	public override void Notify (Brush brush) {
		inputField.text = brush.Food.ToString();
	}

	protected override bool CreateSpecific (string food, out Command cmd) {
		cmd = new BrushChangeFoodCommand(int.Parse(food));
		return true;
	}
}
