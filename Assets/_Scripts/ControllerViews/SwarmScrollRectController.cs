﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwarmScrollRectController : MonoBehaviour, IBeginDragHandler, IEndDragHandler {
	void IBeginDragHandler.OnBeginDrag (PointerEventData eventData) {
		EventManager.TriggerEvent("SwarmScollRectDragStart");
	}

	void IEndDragHandler.OnEndDrag (PointerEventData eventData) {
		EventManager.TriggerEvent("SwarmScollRectDragStop");
	}
}
