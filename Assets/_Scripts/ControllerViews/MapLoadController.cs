﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleFileBrowser;

public class MapLoadController : ButtonController
{
	protected override void Create () {
		EventManager.TriggerEvent("OpenFileDialog");
		FileBrowser.ShowSaveDialog(CreateCommand, TriggerClose, false, "C:\\");
	}

	private void TriggerClose () {
		EventManager.TriggerEvent("CloseFileDialog");
	}

	private void CreateCommand (string path) {
		TriggerClose();
		Command cmd = new MapLoadCommand(path);
		executor.AddCommand(cmd);
	}
}
