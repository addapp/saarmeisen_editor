﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Field))]
public class MapApplyBrushController : ControllerView<Brush>, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler {

	[SerializeField]
	private FieldType fieldType;
	private int food;
	private char swarmID;
	
	[SerializeField]
	private Field field;

	private bool applyBrush;
	private bool brushEnabled;
	private bool gotPointerDown;

	protected override void OnAwake() {
		field = GetComponent<Field>();
		applyBrush = true;
		brushEnabled = true;
	}

	private void LateUpdate() {
		if (gotPointerDown) {
			if (Input.GetMouseButton(0) && applyBrush) {
				CreateMapUnselectCommand();
				CreateMapApplyBrushCommand();
			}
			gotPointerDown = false;
		}
	}

	private void Start () {
		EventManager.StartListening("CloseFileDialog", EnableBrush);
		EventManager.StartListening("OpenFileDialog", DisableBrush);
		EventManager.StartListening("SwarmScollRectDragStop", EnableBrush);
		EventManager.StartListening("SwarmScollRectDragStart", DisableBrush);
		EventManager.StartListening("CameraModeExit", EnableApplyBrush);
		EventManager.StartListening("CameraModeEnter", DisableApplyBrush);
	}

	private void OnDestroy () {
		EventManager.StopListening("CloseFileDialog", EnableBrush);
		EventManager.StopListening("OpenFileDialog", DisableBrush);
		EventManager.StopListening("SwarmScollRectDragStop", EnableBrush);
		EventManager.StopListening("SwarmScollRectDragStart", DisableBrush);
		EventManager.StopListening("CameraModeExit", EnableApplyBrush);
		EventManager.StopListening("CameraModeEnter", DisableApplyBrush);
	}

	private void DisableBrush() {
		brushEnabled = false;
	}

	private void EnableBrush() {
		brushEnabled = true;
	}

	private void EnableApplyBrush() {
		applyBrush = true;
	}

	private void DisableApplyBrush() {
		applyBrush = false;
	}

	public void CreateMapApplyBrushCommand () {
		MapApplyBrushCommand cmd = new MapApplyBrushCommand(field, fieldType, food, swarmID);
		executor.AddCommand(cmd);
	}

	private void CreateMapSelectCommand () {
		MapSelectCommand cmd = new MapSelectCommand(field, fieldType, food, swarmID);
		executor.AddCommand(cmd);
	}

	private void CreateMapUnselectCommand () {
		MapUnselectCommand cmd = new MapUnselectCommand(field);
		executor.AddCommand(cmd);
	}

	void IPointerEnterHandler.OnPointerEnter (PointerEventData eventData) {
		if (!brushEnabled) {
			return;
		}

		if (Input.GetMouseButton(0) && applyBrush) {
			CreateMapApplyBrushCommand();
		} else {
			CreateMapSelectCommand();
		}
	}

	void IPointerDownHandler.OnPointerDown (PointerEventData eventData) {
		if (!brushEnabled) {
			return;
		}

		gotPointerDown = true;
	}

	void IPointerExitHandler.OnPointerExit (PointerEventData eventData) {
		if (!brushEnabled) {
			return;
		}

		CreateMapUnselectCommand();
	}

    public override void Notify(Brush brush)
    {
        fieldType = brush.FieldType;
		food = brush.Food;
		swarmID = brush.SwarmID;
    }
}
