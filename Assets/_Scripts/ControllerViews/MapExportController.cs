﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleFileBrowser;

public class MapExportController : ButtonController {

	private void Start() {
		Button button = GetComponent<Button>();
		EventManager.StartListening("setAuthor", () => button.enabled = true);
		if (PlayerPrefs.HasKey("setAuthor")) {
			button.enabled = true;
		}
	}

	protected override void Create() {
		EventManager.TriggerEvent("OpenFileDialog");
		FileBrowser.ShowSaveDialog(CreateCommand, TriggerClose, false, "C:\\");
	}

	private void TriggerClose () {
		EventManager.TriggerEvent("CloseFileDialog");
	}

	private void CreateCommand (string path) {
		TriggerClose();
		Command cmd = new MapExportCommand(path);
		executor.AddCommand(cmd);
	}
}
