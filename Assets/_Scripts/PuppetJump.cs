﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PuppetJump : MonoBehaviour {

	private Rigidbody body;
	[SerializeField] private float minForce = 5;
	[SerializeField] private float maxForce = 10;

	private void Awake () {
		body = GetComponent<Rigidbody>();
	}

	private void Update () {
		if (Input.GetKeyDown(KeyCode.Space)) {
			float force = Random.Range(minForce, maxForce);
			body.AddForce(Vector3.up * force, ForceMode.Impulse);
		}
	}
}
