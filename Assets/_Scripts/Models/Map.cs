﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using SimpleFileBrowser;
using UnityEngine.Networking;
using System.Security.Cryptography;

public class Map : Model<Map> {

	// how much of the array is currently visible
	[SerializeField] private int height;
	[SerializeField] private int width;

	public Symmetry Symmetry {
		get; set;
	}

	public Field fieldPrefab;

	// the actual dimension of the fields array
	private int maxHeight;
	private int maxWidth;

	public static readonly int MIN_DIM = 2;
	public static readonly int MAX_DIM = 128;

	private System.Random random;

	private Field[,] fields;
	private Tuple<int,int>[][] neighbourIndices = new Tuple<int, int>[][] {
		new Tuple<int,int>[] {
			Tuple.Create(1, 0), Tuple.Create(0, -1), Tuple.Create(-1, -1),
			Tuple.Create(-1, 0), Tuple.Create(-1, 1), Tuple.Create(0, 1)
		},
		new Tuple<int,int>[] {
			Tuple.Create(1, 0), Tuple.Create(1, -1), Tuple.Create(0, -1),
			Tuple.Create(-1, 0), Tuple.Create(0, 1), Tuple.Create(1, 1)
		}
	};

	void Awake() {
		random = new System.Random();
	}

	// Use this for initialization
	void Start () {
		FileBrowser.SetFilters(true, new FileBrowser.Filter("Maps", ".map"));
		FileBrowser.SetDefaultFilter(".map");
		FileBrowser.AddQuickLink("Users", "C:\\Users", null);
		InitMap();
	}

	public void Restart () {
		ResetMap();
		InitMap();
	}

	public void ResetMap () {
		maxHeight = maxWidth = height = width = 0;
		Symmetry = Symmetry.None;

		if (fields != null) {
			foreach (Field field in fields) {
				Destroy(field.gameObject);
			}
		}

		fields = null;
	}

	public void InitMap () {
		InitializeFields(0, 2, 0, 2);
		SetHeight(2);
		SetWidth(2);
	}

	#region ChangingDimensions
	/// <summary>
	/// Initializes some area of fields. 
	/// </summary>
	/// <param name="fromWidth"></param>
	/// <param name="toWidth"></param>
	/// <param name="fromHeight"></param>
	/// <param name="toHeight"></param>
	private void InitializeFields (int fromWidth, int toWidth, int fromHeight, int toHeight) {

		Field[,] tmpFields = new Field[toWidth, toHeight];

		// copy old fields
		for (int x = 0; x < maxWidth; x++) {
			for (int y = 0; y < maxHeight; y++) {
				tmpFields[x, y] = fields[x, y];
			}
		}

		fields = tmpFields;

		// create new fields
		for (int x = fromWidth; x < toWidth; x++) {
			for (int y = fromHeight; y < toHeight; y++) {
				fields[x, y] = Instantiate(fieldPrefab);
				fields[x, y].transform.SetParent(transform);
				fields[x, y].Init(x, y);
			}
		}

		// update maximum bounds for array
		width = maxWidth = toWidth;
		height = maxHeight = toHeight;
	}

	private void SetActiveFields (int activeWidth, int activeHeight) {

		// first deactivate everything that's currently active
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				fields[x, y].gameObject.SetActive(false);
			}
		}

		// then activate the designated fields
		for (int x = 0; x < activeWidth; x++) {
			for (int y = 0; y < activeHeight; y++) {
				fields[x, y].gameObject.SetActive(true);
			}
		}

		// update the bounds for the active area
		width = activeWidth;
		height = activeHeight;
	}
	#endregion

	#region Fields
	public void SetField (Field field, FieldType fieldType, int food, char swarm) {
		field.SetFieldType(fieldType, food, swarm);

		List<Field> symmetricFields = SymmetricFields(field);

		for (int i = 0; i < symmetricFields.Count; i++) {
			Field symmetricField = symmetricFields[i];
			//todo I should calculate the modulo of that. Else when you start with Z you get forbidden characters
			char symmetricSwarm = (char)(swarm + i + 1);
			symmetricField.SetFieldType(fieldType, food, symmetricSwarm);
		}

		NotifyChange();
	}

	public void SelectField (Field field, FieldType fieldType, int food, char swarm) {
		field.SelectField(fieldType, food, swarm);

		foreach (Field symmetricField in SymmetricFields(field)) {
			symmetricField.SelectField(fieldType, food, swarm);
		}
	}

	public void UnselectField (Field field) {
		field.UnselectField();

		foreach (Field symmetricField in SymmetricFields(field)) {
			symmetricField.UnselectField();
		}
	}

	public Field[,] GetFields() {
		return fields;
	}

	public Field GetFieldAt (int x, int y) {
		if ((x < 0 || x >= width) 
			|| (y < 0 || y >= height)) {
			throw new ArgumentException("Wrong coordinates");
		}

		return fields[x, y];
	}

	public List<Field> GetNeighbours (Field field) {
		List<Field> neighbours = new List<Field>(6);
		int parity = field.Y % 2;

		foreach (Tuple<int,int> indices in neighbourIndices[parity]) {
			int neighbourX = modulo(field.X + indices.Item1, width);
			int neighbourY = modulo(field.Y + indices.Item2, height);
			neighbours.Add(fields[neighbourX, neighbourY]);
		}

		return neighbours;
	}
	#endregion

	#region Dimensions
	public int GetHeight () {
		return height;
	}

	/// <summary>
	/// Sets the height of the map.
	/// </summary>
	/// <param name="value">the new height</param>
	public void SetHeight (int value) {
		if (value > maxHeight) {
			SetActiveFields(maxWidth, maxHeight);
			InitializeFields(0, maxWidth, maxHeight, value);
		} else if (value != height) {
			SetActiveFields(width, value);
		}

		NotifyChange();
	}

	public int GetWidth () {
		return width;
	}

	/// <summary>
	/// Sets the width of the map.
	/// </summary>
	/// <param name="value">the new width</param>
	public void SetWidth (int value) {
		if (value > maxWidth) {
			SetActiveFields(maxWidth, maxHeight);
			InitializeFields(maxWidth, value, 0, maxHeight);
		} else if (value != width) {
			SetActiveFields(value, height);
		}

		NotifyChange();
	}
	#endregion

	#region ExportLoad

	public void StartExport(string path) {
		StartCoroutine(Export(path));
	}
	private IEnumerator Export(string path) {
		// check if we already have generated a key for a map with this name
		string author = PlayerPrefs.GetString("author");
		if ("".Equals(author)) {
			throw new InvalidOperationException("Can not export map without setting author.");
		}
		string filename = Path.GetFileName(path);
		byte[] key;
		string keyString = PlayerPrefs.GetString(filename);
		if ("".Equals(keyString)) {
			key = new byte[512];
			random.NextBytes(key);
			keyString = Convert.ToBase64String(key);
			PlayerPrefs.SetString(filename, keyString);
		} else {
			key = Convert.FromBase64String(keyString);
		}

		// width + 1 since we have to append \n to every line, +8 for writing two 3 digit numbers plus newlines
		StringBuilder builder = new StringBuilder((GetWidth() + 1) * GetHeight() + 8);

		builder.Append(GetWidth() + "\n");
		builder.Append(GetHeight() + "\n");

		for (int y = 0; y < GetHeight(); y++) {
			for (int x = 0; x < GetWidth(); x++) {
				Field field = fields[x, y];
				builder.Append(field.ToString());
			}
			builder.Append('\n');
		}

		String map = builder.ToString();

		// also send to Ferdinand's website
		List<IMultipartFormSection> form = new List<IMultipartFormSection> {
			new MultipartFormDataSection("author", author),
			new MultipartFormDataSection("mname", filename),
			new MultipartFormDataSection("map", map),
			new MultipartFormDataSection("auth", key)
		};

		UnityWebRequest postMap = UnityWebRequest.Post("https://saarmeisen.soontm.net/upload.php", form);
		yield return postMap.SendWebRequest();
		//Don't care if it worked for now. We could not write to the file, maybe in a future release
		try {
			File.WriteAllText(path, map);
		} catch (System.UnauthorizedAccessException e) {
			Debug.Log(string.Format("Not authorized to write to {0}", path));
		}
	}

	public void Load(string path) {
		ResetMap();

		string[] map = File.ReadAllLines(path);

		int width = int.Parse(map[0]);
		int height = int.Parse(map[1]);
		InitializeFields(0, width, 0, height);

		if (width < MIN_DIM || width > MAX_DIM) {
			throw new ArgumentException("invalid width");
		}
		if (height < MIN_DIM || height > MAX_DIM) {
			throw new ArgumentException("invalid height");
		}

		if (map.Length != height + 2) {
			throw new ArgumentException("wrong height");
		}

		for (int y = 0; y < map.Length - 2; y++) {
			string line = map[y+2];

			if (line.Length != width) {
				throw new ArgumentException("wrong width");
			}

			for (int x = 0; x < line.Length; x++) {
				ParseChar(x, y, line[x]);
			}
		}

		NotifyChange();
	}

	private void ParseChar(int x, int y, char c) {
		if (c >= '1' && c <= '9') {
			fields[x, y].SetFieldType(FieldType.Normal, c - 48, 'A');
			return;
		}

		if (c >= 'A' && c <= 'Z') {
			fields[x, y].SetFieldType(FieldType.Base, 0, c);
			return;
		}

		switch(c) {
			case '.':
				fields[x, y].SetFieldType(FieldType.Normal, 0, 'A');
				break;
			case '#':
				fields[x, y].SetFieldType(FieldType.Rock, 0, 'A');
				break;
			case '=':
				fields[x, y].SetFieldType(FieldType.Antlion, 0, 'A');
				break;
			default:
				throw new ArgumentException("Invalid character");
		}
	}
	#endregion

	#region Symmetry
	private List<Field> SymmetricFields (Field field) {
		bool horizontal = false;
		bool vertical = false;
		bool diagonal = false;

		switch (Symmetry) {
			case Symmetry.None:
				break;
			case Symmetry.Horizontal:
				horizontal = true;
				break;
			case Symmetry.Vertical:
				vertical = true;
				break;
			case Symmetry.Diagonal:
				diagonal = true;
				break;
			case Symmetry.FourWay:
				horizontal = vertical = diagonal = true;
				break;
			default:
				throw new System.ArgumentException("Unknown enum value: " + Symmetry);
		}

		int x = field.X;
		int y = field.Y;
		List<Field> symmetricFields = new List<Field>();

		if (horizontal) {
			int xH = x;
			int yH = GetHeight() - 1 - y;
			if (y < GetHeight() / 2 && y % 2 == 1) {
				xH = modulo(xH + 1, GetWidth());
			} else if (y >= GetHeight() / 2 && y % 2 == 0) {
				xH = modulo(xH - 1, GetWidth());
			}
			symmetricFields.Add(fields[xH, yH]);
		}
		if (vertical) {
			int xV = GetWidth() - 1 - x;
			int yV = y;
			if (y % 2 == 1) {
				xV = modulo(xV - 1, GetWidth());
			}
			symmetricFields.Add(fields[xV, yV]);
		}
		if (diagonal) {
			int xD = GetWidth() - 1 - x;
			int yD = GetHeight() - 1 - y;
			symmetricFields.Add(fields[xD, yD]);
		}

		return symmetricFields;
	}

	private int modulo(int a, int b) {
		int r = a % b;
		return r < 0 ? r + b : r;
	}
	#endregion
}

