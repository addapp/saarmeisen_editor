﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Symmetry
{
	None, Horizontal, Vertical, Diagonal, FourWay
}