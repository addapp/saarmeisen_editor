﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brush : Model<Brush> {


	[SerializeField] private int food;
	[SerializeField] private char swarmID;
	[SerializeField] private FieldType fieldType;

	public int Food {
		get { return food; }
		set {
			food = value;
			NotifyChange();
		}
	}
	public char SwarmID {
		get { return swarmID; }
		set {
			swarmID = value;
			NotifyChange();
		}
	}
	public FieldType FieldType {
		get { return fieldType; }
		set {
			fieldType = value;
			NotifyChange();
		}
	}

	// Use this for initialization
	void Start () {
		FieldType = FieldType.Normal;
		Food = 0;
		SwarmID = 'A';
	}
}
