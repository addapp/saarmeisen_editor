﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : MonoBehaviour {

	public int X { get; set; }
	public int Y { get; set; }

	[SerializeField] private Sprite normalSprite;
	[SerializeField] private Sprite rockSprite;
	[SerializeField] private Sprite antlionSprite;
	[SerializeField] private Sprite baseSprite;
	[SerializeField] private new SpriteRenderer renderer;
	[SerializeField] private GameObject antPrefab;
	[SerializeField] private GameObject antlionPrefab;
	[SerializeField] private GameObject selectedFieldPrefab;
	private GameObject puppet;
	private GameObject selectedField;

	private FieldType fieldType;

	private int food;
	[SerializeField]
	private TextMesh foodText;
	private char swarmID;
	public static readonly char noSwarmId = 'x';

	[SerializeField]
	private TextMesh swarmText;

	private float width;
	private float height;

	private void Awake () {
		Vector3 extents = normalSprite.bounds.extents;
		width = 2 * extents.x;
		height = 2 * extents.y;
	}

	public void Init (int x, int y) {
		X = x;
		Y = y;
		SetPosition();
		SetFieldType(FieldType.Normal, 0, noSwarmId);
	}

	public void SetPosition () {
		float world_x = X * width;
		if (Y % 2 != 0) {
			world_x += width / 2;
		}

		float world_y = Y * -0.75f * height;

		transform.localPosition = new Vector3(world_x, 0, world_y);
	}

	public void SelectField (FieldType fieldType, int food, char swarmID) {
		UnselectField();
		selectedField = Instantiate(selectedFieldPrefab);
		selectedField.transform.SetParent(transform);
		selectedField.transform.localPosition = Vector3.zero;

		selectedField.GetComponent<Field>().SetFieldType(fieldType, food, swarmID);
	}

	public void UnselectField() {
		if (selectedField != null) {
			Destroy(selectedField);
			selectedField = null;
		}
	}

	public void SetFieldType(FieldType fieldType, int food, char swarmID) {
		this.fieldType = fieldType;
		DisableFood();
		DisableBase();
		DisablePuppet();

		switch (fieldType) {
			case FieldType.Normal:
				SetFood(food);
				SetSprite(normalSprite);
				break;
			case FieldType.Rock:
				SetSprite(rockSprite);
				break;
			case FieldType.Antlion:
				SetPuppet(antlionPrefab);
				SetSprite(antlionSprite);
				break;
			case FieldType.Base:
				SetPuppet(antPrefab);
				SetBase(swarmID);
				SetSprite(baseSprite);
				break;
		}
	}

	private void SetFood(int food) {
		this.food = food;
		foodText.text = food.ToString();

		if (food > 0) {
			foodText.gameObject.SetActive(true);
		}
	}

	private void DisableFood() {
		this.food = 0;
		foodText.gameObject.SetActive(false);
	}

	private void SetBase(char swarmID) {
		this.swarmID = swarmID;
		swarmText.text = swarmID.ToString();
		swarmText.gameObject.SetActive(true);
	}

	private void DisableBase () {
		swarmID = noSwarmId;
		swarmText.gameObject.SetActive(false);
	}

	private void SetSprite(Sprite sprite) {
		renderer.sprite = sprite;
	}

	private void SetPuppet(GameObject puppetPrefab) {
		puppet = Instantiate(puppetPrefab);
		puppet.transform.SetParent(transform);
		puppet.transform.localPosition = Vector3.up * 0.1f;
	}

	private void DisablePuppet () {
		if (puppet != null) {
			Destroy(puppet);
			puppet = null;
		}
	}

	public FieldType GetFieldType() {
		return fieldType;
	}

	public int GetFood() {
		return food;
	}

	public char GetSwarmID() {
		return swarmID;
	}

	public override string ToString () {
		switch(fieldType) {
			case FieldType.Normal:
				return food > 0 ? food.ToString() : ".";
			case FieldType.Rock:
				return "#";
			case FieldType.Base:
				return swarmID.ToString();
			case FieldType.Antlion:
				return "=";
			default:
				throw new System.ArgumentException("Fieldtype with unknown value " + fieldType);
		}
	}
}

public enum FieldType
{
	Normal, Rock, Base, Antlion
}
