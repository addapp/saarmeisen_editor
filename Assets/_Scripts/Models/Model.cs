﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Model<T> : MonoBehaviour where T : Model<T> {

	[SerializeField]
	protected List<ControllerView<T>> views = new List<ControllerView<T>>();

	public void Register(ControllerView<T> controllerView) {
		views.Add(controllerView);
	}
	
	protected void NotifyChange () {
		foreach (ControllerView<T> controllerView in views) {
			controllerView.Notify((T)this);
		}
	}
}
