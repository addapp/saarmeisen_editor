﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : ControllerView<Map> {

	//General
	private Camera cam;
	private Transform pivot;
	private Vector3 originalPosition;
	[SerializeField] private bool readInput;
	[SerializeField] private Bounds pivotBounds;
	[SerializeField] private Vector3 boundsMargin;

	//Dragging
	private Vector3 lastRayCast;
	private Vector3 newRayCast;
	private Vector3 distance;
	private bool dragMode;
	private delegate void DragType (); //delegate for either FragByRayCast or DragByInputAxis
	private DragType dragType;
	[SerializeField] private float dragByInputAxisSpeed = 20f;
	[SerializeField] private LayerMask floorMask;

	//Translation
	public float translationSpeed = 10f;

	////Rotation
	//float rotation;
	//public float rotationSpeed = 100f;

	//Zoom
	[SerializeField] private float minHeight = 10f;
	[SerializeField] private float maxHeight = 200f;
	[SerializeField] private float zoomSpeed = 1000f;
    private bool alternativeDragMode;

    protected override void OnAwake () {
		pivot = transform;
	}

	void OnEnable () {
		dragType = DragByRayCast;
	}

	void Start () {
		cam = GetComponentInChildren<Camera>();
		originalPosition = cam.transform.localPosition;
		readInput = true;
		EventManager.StartListening("WriteToInputFieldStop", () => readInput = true);
		EventManager.StartListening("WriteToInputFieldStart", () => readInput = false);
		EventManager.StartListening("CloseFileDialog", () => readInput = true);
		EventManager.StartListening("OpenFileDialog", () => readInput = false);
		EventManager.StartListening("MenuClose", () => readInput = true);
		EventManager.StartListening("MenuOpen", () => readInput = false);
	}

	void Update () {
		if (!readInput) {
			return;
		}
	
		dragType();
		//Rotate();
		Zoom();
		KeyCommands();

		// If The camera pivot moved out of the bounds we clamp it back on the bounds
		// and reset the lastRayCast to the current mouse position so that dragging against the boundary feels more natural
		if (!pivotBounds.Contains(pivot.position)) {
			pivot.position = pivotBounds.ClosestPoint(pivot.position);
			PerformRayCast(out lastRayCast);
		}
	}

	/// <summary>
	/// We start a drag either when the user clicks the right mouse button or the left mouse button while holding down control.
	/// </summary>
	/// <returns>Whether to start a drag.</returns>
	private bool CheckStartDrag() {
		return Input.GetMouseButtonDown(1)
			 	|| (Input.GetMouseButtonDown(0) && alternativeDragMode);
	}

	/// <summary>
	/// We start a drag either when the user clicks the right mouse button or the left mouse button while holding down control.
	/// </summary>
	/// <returns>Whether to start a drag.</returns>
	private bool CheckContinueDrag () {
		return dragMode
				&& (Input.GetMouseButton(1) || Input.GetMouseButton(0));
	}

	private bool PerformRayCast (out Vector3 vector) {
		Ray camRay = cam.ScreenPointToRay(Input.mousePosition); ;
		RaycastHit camRayHit;
		//Debug.Log("Performing RayCast");
		//Debug.Log(floorMask.value);
		Debug.DrawRay(camRay.origin, camRay.direction * 100);

		if (Physics.Raycast(camRay, out camRayHit, Mathf.Infinity, floorMask)) {
			//Debug.Log("Hit something");
			vector = camRayHit.point;
			return true;
		} else {
			//Debug.Log("Hit nothing");
			vector = Vector3.zero;
			return false;
		}
	}

	/// <summary>
	/// In DragByRayCast I perform a Raycast on every frame and move the gameObject the main Camera is attached to
	/// to the hitpoint of the Raycast
	/// While dragging, the cursor alawys stays in the same spot, thus it feels more natural and pleasing to the eye
	/// </summary>
	void DragByRayCast () {
		alternativeDragMode = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
		
		bool startDragMode = CheckStartDrag() && PerformRayCast(out lastRayCast);   //when I start I get the point on the map my cursor is currently at
		
		if (!dragMode && startDragMode) {
			dragMode = true;
			EventManager.TriggerEvent("CameraModeEnter");
		}

		bool continueDragMode = CheckContinueDrag() && PerformRayCast(out newRayCast);
		if (continueDragMode) {  //each frame I get the new point my cursor is at
			Debug.Log("Continuing drag");
			distance = lastRayCast - newRayCast;    //negative distance between the points, as I have to move the camera in the oppposite direction
			Debug.DrawLine(lastRayCast, newRayCast);
			distance.y = 0;
			//Debug.Log(distance);
			pivot.transform.Translate(distance, Space.World);   //space world because I calculate the distance on a global scale
		} else if (dragMode && !continueDragMode) {
			dragMode = false;
			EventManager.TriggerEvent("CameraModeExit");
		}
	}

	/// <summary>
	/// In DragByInputAxis I read the input of the mouse X and Y-axis to calculate the distance I have to move the 
	/// gameObject.
	/// As the camera is always inclinded, the cursor is more distached from the map, doesn't look as good.
	/// But make it an option to switch to this dragtype.
	/// </summary>
	void DragByInputAxis () {
		if (Input.GetMouseButton(1)) {
			Vector3 distance = new Vector3(
				Input.GetAxis("Mouse X") * dragByInputAxisSpeed * Time.deltaTime,
				0,
				Input.GetAxis("Mouse Y") * dragByInputAxisSpeed * Time.deltaTime
				);

			pivot.transform.position -= distance;
		}
	}

	//I just read the value of the rotation axis and rotate the pivot
	//void Rotate () {
	//	rotation = Input.GetAxis("Rotation");

	//	pivot.transform.Rotate(0, rotation * rotationSpeed * Time.deltaTime, 0);
	//}

	//read the scrollWheel axis and change the height of the camera
	void Zoom () {
		float zoom = Input.GetAxis("Mouse ScrollWheel");

		Vector3 tempPos = cam.transform.localPosition;
		cam.transform.Translate(0, 0, zoom * zoomSpeed * Time.deltaTime, Space.Self);

		if (cam.transform.localPosition.y < minHeight || cam.transform.localPosition.y > maxHeight)
			cam.transform.localPosition = tempPos;
	}

	void CenterCamera () {
		pivot.transform.position = Vector3.zero;
	}

	//void ResetRotation () {
	//	pivot.transform.rotation = Quaternion.identity;
	//}

	void ResetZoom () {
		cam.transform.localPosition = originalPosition;
	}

	void Translate () {
		Vector3 translation = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
		if (translation.Equals(Vector3.zero)) {
			return;
		}
		if (PlayerPrefs.HasKey("invertCamera")) {
			translation = -translation;
		}
		translation.Normalize();
		pivot.transform.Translate(translation * translationSpeed * Time.deltaTime);
	}

	void KeyCommands () {
		//Movement
		Translate();

		//ResetAll
		if (Input.GetKeyDown(KeyCode.X)) {
			Debug.Log("Reset Camera");
			CenterCamera();
			//ResetRotation();
			ResetZoom();
		}

		//Center the camera
		if (Input.GetKeyDown(KeyCode.C)) {
			Debug.Log("Reset Position");
			CenterCamera();
		}

		////Reset rotation
		//if (Input.GetKeyDown(KeyCode.V)) {
		//	Debug.Log("Reset Rotation");
		//	ResetRotation();
		//}

		//Reset zoom/height
		if (Input.GetKeyDown(KeyCode.B)) {
			Debug.Log("Reset Zoom");
			ResetZoom();
		}
	}

	public override void Notify (Map map) {
		Vector3 upperLeft = map.GetFieldAt(0, 0).transform.position;
		Vector3 lowerRight = map.GetFieldAt(map.GetWidth() - 1, map.GetHeight() - 1).transform.position;
		Vector3 midPoint = Vector3.Lerp(upperLeft, lowerRight, 0.5f);
		Vector3 extent = new Vector3(lowerRight.x, lowerRight.y, -lowerRight.z) + 2 * boundsMargin;
		
		pivotBounds = new Bounds(midPoint, extent);
	}
}
