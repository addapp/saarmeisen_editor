﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapApplyBrushCommand : MapCommand {

	private Field field;
	private FieldType fieldType, oldFieldType;
	private int food, oldFood;
	private char swarmID, oldSwarmID;

	public MapApplyBrushCommand(Field field, FieldType fieldType, int food, char swarmID) {
		this.field = field;
		this.fieldType = fieldType;
		this.food = food;
		this.swarmID = swarmID;
	}

	protected override void ExecuteSpecific() {
		oldFieldType = field.GetFieldType();
		oldFood = field.GetFood();
		oldSwarmID = field.GetSwarmID();
		map.SetField(field, fieldType, food, swarmID);
	}
}
