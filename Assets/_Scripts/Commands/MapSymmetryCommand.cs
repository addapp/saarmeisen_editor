﻿
public class MapSymmetryCommand : MapCommand {

	private Symmetry symmetry;
	//todo do I even need an old value here. If I always update all my controllerviews then then it can only ever be !symmetric I think

	public MapSymmetryCommand(Symmetry symmetry) {
		this.symmetry = symmetry;
	}

	protected override void ExecuteSpecific () {
		map.Symmetry = symmetry;
	}
}
