﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapResizeHeightCommand : MapCommand {

	private int height;
	private int previousHeight;

	public MapResizeHeightCommand (int height) {
		this.height = height;
	}

	protected override void ExecuteSpecific () {
		previousHeight = map.GetHeight();
		map.SetHeight(height);
	}
}
