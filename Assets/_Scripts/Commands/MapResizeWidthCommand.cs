﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapResizeWidthCommand : MapCommand {

	private int width;
	private int previousWidth;

	public MapResizeWidthCommand (int width) {
		this.width = width;
	}

	protected override void ExecuteSpecific () {
		previousWidth = map.GetWidth();
		map.SetWidth(width);
	}
}
