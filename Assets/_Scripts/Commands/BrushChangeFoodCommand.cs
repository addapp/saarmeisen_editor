﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushChangeFoodCommand : BrushCommand
{
	private int food;
	private int oldFood;

	public BrushChangeFoodCommand (int food) {
		this.food = food;
	}

	protected override void ExecuteSpecific () {
		oldFood = brush.Food;
		brush.Food = food;
	}
}
