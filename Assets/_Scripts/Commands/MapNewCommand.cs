﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapNewCommand : MapCommand {
	protected override void ExecuteSpecific () {
		map.Restart();
	}
}
