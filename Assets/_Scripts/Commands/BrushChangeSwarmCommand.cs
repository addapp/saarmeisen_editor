﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushChangeSwarmCommand : BrushCommand
{
	private char swarmID;
	private char oldSwarmID;

	public BrushChangeSwarmCommand (char swarmID) {
		this.swarmID = swarmID;
	}

	protected override void ExecuteSpecific () {
		oldSwarmID = brush.SwarmID;
		brush.SwarmID = swarmID;
	}
}
