﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MapCommand : Command {

	protected Map map;

	public void Execute() {
		map = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>();
		ExecuteSpecific();
	}

	protected abstract void ExecuteSpecific ();
}
