﻿using SimpleFileBrowser;

public class MapExportCommand : MapCommand
{
	private string path;

	public MapExportCommand(string path) {
		this.path = path;
	}

	protected override void ExecuteSpecific () {
		map.StartExport(path);
	}
}
