﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapUnselectCommand : MapCommand {

	private Field field;

	public MapUnselectCommand (Field field) {
		this.field = field;
	}

	protected override void ExecuteSpecific() {
		map.UnselectField(field);
	}
}
