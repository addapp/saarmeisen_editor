﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandExecutor : MonoBehaviour {

	private List<Command> commands = new List<Command>();

	public void AddCommand (Command command) {
		commands.Add(command);
		command.Execute();
	}
}
