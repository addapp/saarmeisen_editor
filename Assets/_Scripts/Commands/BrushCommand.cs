﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BrushCommand : Command {

	protected Brush brush;

	public void Execute() {
		brush = GameObject.FindGameObjectWithTag("Brush").GetComponent<Brush>();
		ExecuteSpecific();
	}

	protected abstract void ExecuteSpecific ();
}
