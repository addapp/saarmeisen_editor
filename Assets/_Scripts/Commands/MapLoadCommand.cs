﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleFileBrowser;

public class MapLoadCommand : MapCommand {

	private string path;

	public MapLoadCommand (string path) {
		this.path = path;
	}

	protected override void ExecuteSpecific () {
		map.Load(path);
	}
}
