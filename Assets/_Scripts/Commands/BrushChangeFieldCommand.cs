﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushChangeFieldCommand : BrushCommand {

	private FieldType fieldType;
	private FieldType oldFieldType;

	public BrushChangeFieldCommand(FieldType fieldType) {
		this.fieldType = fieldType;
	}

	protected override void ExecuteSpecific () {
		oldFieldType = brush.FieldType;
		brush.FieldType = fieldType;
	}
	
}
